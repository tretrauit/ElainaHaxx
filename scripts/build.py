#!/usr/bin/python3

from gitignore_parser import parse_gitignore
from pathlib import Path
from datetime import datetime
from subprocess import run
import shutil
import zipfile
import toml
import json

WEBSITE = "https://tretrauit.gitlab.io/elainahaxx"

def zip_directory(dir, zip, gitignore):
    print(f"Zipping file: {zip.filename}")
    for child in dir.rglob("*"):
        if not gitignore(child):
            print(f"Writing {child}")
            zip.write(child)
        else:
            print(f"Ignoring {child}")
    print(f"Zipped sucessfully into {zip.filename}")


def generate_metadata(game_version: str, loader_version: str, file_name: str) -> str:
    branch_name = run(["git", "branch", "--show-current"], capture_output=True).stdout.decode("utf-8").strip()
    return json.dumps({
        "game_version": game_version,
        "loader_version": loader_version,
        "file_name": file_name,
        "download": f"{WEBSITE}/cdn/mmc/{branch_name}/{file_name}",
    }, indent=4)


def main():
    artifacts_folder = Path("./artifacts/")
    artifacts_folder.mkdir(exist_ok=True)
    mmc_path = Path("./src/polymc/")
    packwiz_path = Path("./src/packwiz/")
    if not mmc_path.exists():
        print("Prism source folder not found. Please run this script from the root of the repository.")
        return
    print("Found Prism source, begin building Prism instance...")
    print("Reading modpack info...")
    pack_info = toml.load(packwiz_path.joinpath("pack.toml").open("r"))
    version = pack_info["versions"]
    game_version = version["minecraft"]
    quilt_version = version["quilt"]
    mmc_filename = f"ElainaHaxx-{game_version}-{quilt_version}-prism-{datetime.now().strftime('%Y%m%d')}{datetime.now().strftime('%H%M%S')}.zip"
    print(f"Packing into {mmc_filename}")
    mmc_gitignore = parse_gitignore(mmc_path.joinpath(Path(".gitignore")))
    with zipfile.ZipFile(mmc_filename, 'w', zipfile.ZIP_STORED) as mmc_zip:
        zip_directory(mmc_path, mmc_zip, mmc_gitignore)
    print("Done, copying to artifacts folder...")
    shutil.move(mmc_filename, artifacts_folder)
    print("Sucessfully copied to artifacts folder.")
    print("Generating metadata...")
    metadata = generate_metadata(game_version, quilt_version, mmc_filename)
    metadata_file = artifacts_folder.joinpath("metadata.json")
    with metadata_file.open("w") as f:
        f.write(metadata)
    print("Sucessfully generated metadata.")

if __name__ == "__main__":
    main()
