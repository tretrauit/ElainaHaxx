# ElainaHaxx

A modpack aiming to increase your performance in Minecraft

+ This modpack is currently running in Minecraft 1.19 (Quilt)

## Installation

Currently this modpack only supports MultiMC/PolyMC, although there are plans for other launchers

### MultiMC

+ Download the bootstrapper instance from [here](https://tretrauit.gitlab.io/elainahaxx/cdn/mmc/1.19.x/ElainaHaxx-latest.zip) or [GitLab CI](https://gitlab.com/tretrauit/ElainaHaxx/-/pipelines) and add it to PolyMC/MultiMC
    > The source of the MultiMC/PolyMC instance can be found at [src/polymc/](./src/polymc/)

### FAQ

#### What's `.pwfrzd` and `.exfrzd` extension?

A: It's a file extension used by [pywiz.py](./scripts/pywiz.py) to indicate that the mod is disabled (`.pwfrzd` is PackWiz mod, while `.exfrzd` is an external mod (usually GitHub CI ones))

#### Why not the good old `.disabled`?

A: People may mistakenly think that if they enable the `.disabled` mod in MultiMC it'll enable the mod but actually, it doesn't.
